require.config({
    baseUrl: "js/vendor",
    paths: {
        model: "../app/model",
        view: "../app/view",
        controller: "../app/controller",
        app: "../app"
    },
    shim: {
        'backbone': {
            deps: ['underscore', 'jquery'],
            exports: 'Backbone'
        },
        'underscore': {
            exports: '_'
        }
    }
});

require(['jquery', 'backbone', 'app/router'], function ($, Backbone, Router) {

    var router = new Router();
    Backbone.history.start();

});
