define(function(require) {

    "use strict";

    var $ = require('jquery'),
        _ = require('underscore'),
        View = require('app/view'),

        tpl = require('text!view/Menu.html'),
        template = _.template(tpl);

    return View.extend({

        delegateEvents: function() {

            console.log("DELEGATE Menu");

        },

        render: function() {

            this.$el.html(template);
            return this;

        }

    });

});
