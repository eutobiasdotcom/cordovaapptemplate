define(function(require) {

    "use strict";

    var $ = require('jquery'),
        _ = require('underscore'),
        View = require('app/view'),

        tpl = require('text!view/Mapa.html'),
        template = _.template(tpl);

    return View.extend({

        delegateEvents: function() {

            console.log("DELEGATE Mapa");

        },

        render: function() {

            this.$el.html(template);
            return this;

        }

    });

});
