define(function(require) {

    "use strict";

    var $ = require('jquery');
    
    var Backbone = require('backbone');
    var Home = require('app/view/Home');
    var Menu = require('app/view/Menu');
    var Casa = require('app/view/Casa');
    var Personagens = require('app/view/Personagens');
    var Jogos = require('app/view/Jogos');
    var Mapa = require('app/view/Mapa');

    var $body = $('body');
    var homeView = new Home( {el : $body } );
    var menuView = new Menu( {el : $body } );
    var casaView = new Casa( {el : $body } );
    var mapaView = new Mapa( {el : $body } );
    var personagensView = new Personagens( {el : $body } );
    var jogosView = new Jogos( {el : $body } );

    return Backbone.Router.extend({

        routes: {
            "": "home",
            "menu": "menu",
            "casa_mobile": "casa_mobile",
            "personagens_mobile": "personagens_mobile",
            "jogos_mobile": "jogos_mobile",
            "mapa": "mapa",
        },

        home: function() {

            // console.log(this);
            homeView.render();

        },

        menu: function() {

            menuView.render();


        },

        mapa: function() {

            mapaView.render();


        },

        casa_mobile: function() {

            casaView.render();


        },

        personagens_mobile: function() {

            personagensView.render();

        },

        jogos_mobile: function() {

            jogosView.render();

        },

    });

});
