define(function(require) {

    "use strict";

    var Backbone = require('backbone');

    return Backbone.View.extend({

        delegateEvents: function() {

            console.log("DELEGATE ALL VIEWS");

        }

        // _test : function() {
        //     console.log(this);
        // }

    });

});